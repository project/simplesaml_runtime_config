<?php
/**
 * @file Helper script to ease integration with SimpleSAMLphp.
 *
 * Before you instantiate SimpleSamlRuntimeConfig, you must define the path
 * to the JSON file containing the configuration data in a constant called
 * SIMPLESAML_RUNTIME_CONFIG_FILE. The class will throw an exception otherwise.
 */

/**
 * Simple class so that we can store the data and then let people retrieve it
 * through various methods.
 */
class SimpleSamlRuntimeConfig {

  protected $simpleSamlRuntimeConfig;

  public function __construct($file_path = NULL) {
    if (!defined('SIMPLESAML_RUNTIME_CONFIG_FILE') || !file_exists(SIMPLESAML_RUNTIME_CONFIG_FILE)) {
      throw new \Exception('Before including the SimpleSAML Runtime Configuration helper script, please define a constant named SIMPLESAML_RUNTIME_CONFIG_FILE set to the absolute path of the JSON file output by the module. It is called simplesaml_runtime_config.json and located in the simplesaml_runtime_config subdirectory of your private file directory.');
    }
    if (empty($file_path)) {
      $file_path = SIMPLESAML_RUNTIME_CONFIG_FILE;
    }
    $this->simpleSamlRuntimeConfig = json_decode(file_get_contents($file_path), TRUE);
  }

  /**
   * Returns the raw config.
   *
   * @return array
   */
  public function getConfig() {
    return $this->simpleSamlRuntimeConfig;
  }

  /**
   * Allows changing the contents of the configuration property wholesale.
   *
   * @param array $simplesaml_runtime_config
   */
  public function setConfig($simplesaml_runtime_config) {
    $this->simpleSamlRuntimeConfig = $simplesaml_runtime_config;
  }

  public function configureDatabase(&$db) {
    $db = $this->simpleSamlRuntimeConfig['database'];
  }

  public function configureIdp(&$idp) {
    $idp = $this->simpleSamlRuntimeConfig['simplesaml_runtime_config_entityId'];
  }

  public function configureMetadata(&$metadata) {
    // This is a case where we might only want to change certain things from
    // what we have in code. So we iterate over each item and only set it if it
    // is not empty in the file. We check for <BLANK> in case they want to unset
    // a value specifically.
    $prefix = 'simplesaml_runtime_config_';
    $suffixes = array(
      'entityId',
      'SingleSignOnService',
      'SingleLogoutService',
      'X509Certificate',
    );
    $variables = $values = array();
    foreach ($suffixes as $suffix) {
      $variables[$prefix . $suffix] = $suffix;
    }

    // To do this properly, we need the entityId. Grab that manually.
    $entity_id = $this->simpleSamlRuntimeConfig[$prefix . 'entityId'];
    // Without a valid entityId, we can do nothing. So just do nothing if it's
    // not set.
    if (!$entity_id) {
      return;
    }

    // Ensure the array is initialized.
    if (empty($metadata[$entity_id])) {
      $metadata[$entity_id] = array();
    }

    foreach ($variables as $json_name => $saml_name) {
      $json_value = $this->simpleSamlRuntimeConfig[$json_name];
      if ($json_value) {
        // We check for <BLANK> in case they want to unset a value specifically.
        if ($json_value === '<BLANK>') {
          $json_value = '';
        }

        // Some fields need special handling.
        switch ($saml_name) {
          case 'X509Certificate':
            $metadata[$entity_id]['keys'] = array(
              array(
                'encryption' => !!$this->simpleSamlRuntimeConfig["{$prefix}X509Certificate_encryption"],
                'signing' => !!$this->simpleSamlRuntimeConfig["{$prefix}X509Certificate_signing"],
                'type' => 'X509Certificate',
                'X509Certificate' => $json_value,
              ),
            );
            break;
          default:
            $metadata[$entity_id][$saml_name] = $json_value;
            break;
        }
      }
    }
  }

}

