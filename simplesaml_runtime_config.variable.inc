<?php
/**
 * Implements hook_variable_info().
 *
 * @todo: Make it easier to add more config variables if need later. Ideally, put the names and variable definitions in a separate variable so that anyone can call that function to get a list of the variables that actually get written to the file.
 */
function simplesaml_runtime_config_variable_info($options) {
  $variable = array();

  $variable['simplesaml_runtime_config_entityId'] = array(
    'title' => t('entityID', array(), $options),
    'type' => 'string',
    'default' => '',
    'required' => TRUE,
  );

  $variable['simplesaml_runtime_config_SingleSignOnService'] = array(
    'title' => t('SingleSignOnService', array(), $options),
    'type' => 'string',
    'default' => '',
  );

  $variable['simplesaml_runtime_config_SingleLogoutService'] = array(
    'title' => t('SingleLogoutService', array(), $options),
    'type' => 'string',
    'default' => '',
  );

  $variable['simplesaml_runtime_config_X509Certificate'] = array(
    'title' => t('X509Certificate', array(), $options),
    'type' => 'text',
    'default' => '',
  );

  $variable['simplesaml_runtime_config_X509Certificate_encryption'] = array(
    'title' => t('X509Certificate: Encryption?'),
    'type' => 'boolean',
    'default' => FALSE,
  );

  $variable['simplesaml_runtime_config_X509Certificate_signing'] = array(
    'title' => t('X509Certificate: Signing?'),
    'type' => 'boolean',
    'default' => FALSE,
  );

  return $variable;
}

/**
 * Implements hook_variable_settings_form_alter().
 */
function simplesaml_runtime_config_variable_settings_form_alter(&$form, &$form_state, $form_id) {
  // Add our submit handler to generate the JSON file.
  if ($form['#variable_module_form'] == 'simplesaml_runtime_config') {
    module_load_include('inc', 'simplesaml_runtime_config', 'simplesaml_runtime_config.admin');

    $json_exists = file_exists(SIMPLESAML_RUNTIME_CONFIG_DEFAULT_FILE_URI);
    $exists_message = t('Runtime configuration exists at %path (%real_path)', array(
      '%path' => SIMPLESAML_RUNTIME_CONFIG_DEFAULT_FILE_URI,
      '%real_path' => drupal_realpath(SIMPLESAML_RUNTIME_CONFIG_DEFAULT_FILE_URI)
    ));
    $empty_message = t('Runtime configuration not yet saved.');
    $form['file_status'] = array(
      '#type' => 'fieldset',
      '#title' => t('Runtime configuration status'),

      'file' => array(
        '#prefix' => '<strong>',
        '#markup' => $json_exists ? $exists_message : $empty_message,
        '#suffix' => '</strong>',
      ),
    );
    $form['#submit'][] = 'simplesaml_runtime_config_settings_form_submit';
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete file'),
    );
  }
}
