SimpleSAML Runtime Configuration
=================================

Dependencies:

- Variable module

Requirements:

- A configured SimpleSAMLphp instance, which generally involves the simplesamlphp_auth Drupal
module. It is not a hard requirement of this module, but you won't get very far without it.
- A configured SAML 2.0 remote IDP. If you place your metadata in metadata/saml20-idp-remote.php,
then this applies to you.

Allows dynamic configuration of the following fields:

- entityId
- SingleSignOnService
- SingleLogoutService
- X509Certificate, and these sub-attributes:
  - signing
  - encryption

The module generates a JSON file containing the values in private storage. You can then manually
integrate this with your SimpleSAML configuration. The file is located at
private://simplesaml_runtime_config/simplesaml_runtime_config.json. Include it into your
configuration and run `json_decode` on it. Or see scripts/simplesaml_runtime_config.php for a useful
helper class that simply requires defining the path to the file in your SimpleSAMLphp configuration
and contains methods to adjust database (config/config.php), SP (config/authsources.php), and
remote IdP (SAML 2.0, saml20-idp-remote.php) configuration for you.

See the project page https://drupal.org/project/simplesaml_runtime_config for information on how to
integrate this with your SimpleSAMLphp installation.

## Note regarding Pantheon

Pantheon on Drupal 7 injects database configuration directly into Drupal. This module will not be
able to read your database configuration and include it in the JSON file unless you do the following
(involves changing your settings.php):

https://pantheon.io/docs/read-environment-config/#drupal-7-and-drupal-8
