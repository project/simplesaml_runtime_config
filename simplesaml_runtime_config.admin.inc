<?php

/**
 * @file
 */

/**
 * Form builder.
 *
 * See simplesaml_runtime_config.variable.inc
 * @see variable_module_form()
 */
function simplesaml_runtime_config_settings_form() {
  return drupal_get_form('variable_module_form', 'simplesaml_runtime_config');
}

function simplesaml_runtime_config_settings_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] !== 'edit-delete') {
    // Generate contents for JSON file we're going to output.
    $contents = array();

    foreach ($form['#variable_edit_form'] as $variable_name) {
      switch ($variable_name) {
        // X509Certificate is finnicky: it's ok with LF, but not
        // CRLF.
        case 'simplesaml_runtime_config_X509Certificate':
          $contents[$variable_name] = str_replace("\r\n", "\n", $form_state['values'][$variable_name]);
          break;

        default:
          $contents[$variable_name] = $form_state['values'][$variable_name];
          break;
      }
    }

    // Include some configuration info from the site so that SimpleSAMLphp
    // doesn't have to reconstruct this.
    global $databases, $base_url, $base_secure_url, $is_https;

    // If the user is on a service like Pantheon, which injects database
    // configuration, $databases might be empty.
    // We only include the standard connection, since this is what SimpleSAMLphp
    // uses.
    $main_database = !empty($databases) ? $databases['default']['default'] : NULL;

    // SimpleSAMLphp might need the correct URL to send the user back to, and
    // so we want to make sure we have the right protocol.
    $primary_url = $is_https ? $base_secure_url : $base_url;

    list($contents['database'], $contents['base_url']) = array(
      $main_database,
      // This makes it easy for SimpleSAMLphp to include our helper script.
      $primary_url,
    );

    // Make sure our target directory exists.
    $save_directory = SIMPLESAML_RUNTIME_CONFIG_DEFAULT_FILE_DIR;
    $directory_ok = file_prepare_directory($save_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

    if ($directory_ok) {
      // Replace the JSON export file. This updates the DB if needed.
      $file = file_save_data(json_encode($contents), SIMPLESAML_RUNTIME_CONFIG_DEFAULT_FILE_URI, FILE_EXISTS_REPLACE);

      if ($file) {
        // Mark this module as using the file so it doesn't get deleted. We just
        // delete and re-add to avoid having to mess with file_usage_list().
        file_usage_delete($file, 'simplesaml_runtime_config', NULL, NULL, 0);

        // We just use the module name for everything since this is our only
        // file.
        file_usage_add($file, 'simplesaml_runtime_config', 'simplesaml_runtime_config', 1);

        $actual_location = drupal_realpath($file->uri);
        drupal_set_message(t("Runtime configuration written to @actual_location", array('@actual_location' => $actual_location)), 'success');
        return;
      }
    }

    // If file still not saved, then we obviously couldn't save it.
    drupal_set_message(t("Unable to save runtime configuration to @default_location! Please ensure that you have configured private files and that your webserver can write to the directory.", array('@default_location' => SIMPLESAML_RUNTIME_CONFIG_DEFAULT_FILE_URI)), 'error');
  }
  else {
    // Delete the saved JSON file (if there is one).
    if (file_exists(SIMPLESAML_RUNTIME_CONFIG_DEFAULT_FILE_URI)) {
      $existing_fid = db_query('select fid from {file_managed} where uri = :uri', array(':uri' => SIMPLESAML_RUNTIME_CONFIG_DEFAULT_FILE_URI))->fetchField();
      if ($existing_fid) {
        $existing_file = file_load($existing_fid);
        file_usage_delete($existing_file, 'simplesaml_runtime_config', NULL, NULL, 0);
        file_delete($existing_file);
      }
      else {
        // Unclear how this became unmanaged, but we can delete that too.
        file_unmanaged_delete(SIMPLESAML_RUNTIME_CONFIG_DEFAULT_FILE_URI);
      }
    }
    drupal_set_message(t('SimpleSAML runtime configuration file deleted from the file system. To regenerate it, click <em>Save configuration</em>.'));
  }
}
